package com.chatapp.persistence;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;

import com.chatapp.application.common.UserDto;
import com.chatapp.application.plugins.UserStore;

public class UserSharedPref implements UserStore {
  private static final String EMPTY = "";
  private static final String LOGGED_USER = "logged_user";

  private SharedPreferences sharedPref;

  public UserSharedPref(AppCompatActivity activity) {
    sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
  }

  @Override public boolean contains(UserDto userDto) {
    return (usernameExist(userDto.getUsername()) && isAuthenticated(userDto));
  }

  @Override public void save(UserDto dto) {
    SharedPreferences.Editor editor = sharedPref.edit();
    editor.putString(dto.getUsername(), dto.getPassword());
    editor.putString(LOGGED_USER, dto.getUsername());
    editor.apply();
  }

  @Override
  public void setLoggedUser(UserDto userDto) {
    SharedPreferences.Editor editor = sharedPref.edit();
    editor.putString(LOGGED_USER, userDto.getUsername());
    editor.apply();
  }

  @Override
  public UserDto getLoggedUser() {
    if (sharedPref.contains(LOGGED_USER)) {
      String username = sharedPref.getString(LOGGED_USER, EMPTY);
      String password = sharedPref.getString(username, EMPTY);

      return new UserDto(username, password);
    } else {
      return UserDto.ERROR;
    }
  }

  private boolean usernameExist(String username) {
    return sharedPref.contains(username);
  }

  private boolean isAuthenticated(UserDto userDto) {
    String password = sharedPref.getString(userDto.getUsername(), EMPTY);
    return userDto.getPassword().equals(password);
  }

}
