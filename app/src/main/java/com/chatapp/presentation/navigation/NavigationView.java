package com.chatapp.presentation.navigation;

import android.support.v4.app.Fragment;

public interface NavigationView {
    void OnNavigateToScreen(Fragment f);
}
