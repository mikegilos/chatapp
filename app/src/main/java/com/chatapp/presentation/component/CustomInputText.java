package com.chatapp.presentation.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chatapp.R;

public class CustomInputText extends LinearLayout {

    private String textHint;
    private int inputType;
    private int maxLength;
    private int minLength;
    private TextView errorPrompt;
    private EditText inputText;

    public CustomInputText(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomInputText(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        inflate(getContext(), R.layout.custom_input_text_layout, this);

        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.CustomInputText);

        try {
            textHint = ta.getString(R.styleable.CustomInputText_hint);
            inputType = ta.getInt(R.styleable.CustomInputText_inputType, InputType.TYPE_CLASS_TEXT);

            int defaultMinLength = getResources().getInteger(R.integer.min_input_length);
            minLength = ta.getInt(R.styleable.CustomInputText_minLength, defaultMinLength);
        } finally {
            ta.recycle();
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        applyEditTextAttrs();
        initErrorPrompt();
    }

    private void initErrorPrompt() {
        errorPrompt = findViewById(R.id.error_prompt);
        errorPrompt.setVisibility(View.GONE);
    }

    private void applyEditTextAttrs() {
        inputText = findViewById(R.id.input_text);
        inputText.setHint(textHint);
        inputText.setInputType(InputType.TYPE_CLASS_TEXT | inputType);
    }

    public String getText() {
        return inputText.getText().toString();
    }

    public void enableErrorPrompt(boolean show) {
        errorPrompt.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
