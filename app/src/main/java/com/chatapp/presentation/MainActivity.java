package com.chatapp.presentation;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.chatapp.R;
import com.chatapp.presentation.common.ToolbarView;
import com.chatapp.presentation.main.MainFragment;
import com.chatapp.presentation.navigation.NavigationView;
import com.chatapp.presentation.signup.SignupFragment;

public class MainActivity extends AppCompatActivity implements NavigationView, ToolbarView {

  private Button logoutBtn;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    initLogoutBtn(toolbar);
    setInitialScreen(savedInstanceState);
  }

  private void initLogoutBtn(Toolbar toolbar) {
    logoutBtn = toolbar.findViewById(R.id.logout_btn);
    logoutBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        OnNavigateToScreen(new SignupFragment());
        enableLogoutButton(false);
      }
    });

    enableLogoutButton(false);
  }

  private void setInitialScreen(Bundle savedInstanceState) {
    if (findViewById(R.id.fragment_container) != null && savedInstanceState == null) {
      OnNavigateToScreen(new MainFragment());
    }
  }

  @Override
  public void OnNavigateToScreen(Fragment f) {
    getSupportFragmentManager().beginTransaction()
        .replace(R.id.fragment_container, f).commit();
  }

  @Override
  public void enableLogoutButton(boolean enable) {
    logoutBtn.setVisibility(enable ? View.VISIBLE : View.GONE);
  }
}
