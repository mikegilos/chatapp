package com.chatapp.presentation.main;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chatapp.R;
import com.chatapp.presentation.login.LoginFragment;
import com.chatapp.presentation.navigation.NavigationView;
import com.chatapp.presentation.signup.SignupFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainFragment extends Fragment {

  private NavigationView navigationView;

  @Override
  public View onCreateView(LayoutInflater inflater,
                           ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_main, container, false);
    ButterKnife.bind(this, view);
    return view;
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof NavigationView) {
      navigationView = (NavigationView) context;
    } else {
      throw new RuntimeException(context.toString()
              + " must implement NavigationView");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    navigationView = null;
  }

  @OnClick(R.id.signup_btn)
  public void signup() {
    navigationView.OnNavigateToScreen(new SignupFragment());
  }

  @OnClick(R.id.login_btn)
  public void login() {
    navigationView.OnNavigateToScreen(new LoginFragment());
  }
}
