package com.chatapp.presentation.chat;

interface ChatView {
  void addMessage(Message message);
}
