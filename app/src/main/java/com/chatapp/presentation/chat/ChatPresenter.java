package com.chatapp.presentation.chat;

import com.chatapp.application.chat.MessageDto;
import com.chatapp.application.chat.OnMessageReceive;
import com.chatapp.application.chat.SendMessageCommand;
import com.chatapp.application.chat.SubscribeOnChatCommand;
import com.chatapp.application.common.UserDto;
import com.chatapp.application.user.GetLoggedUserQuery;
import com.chatapp.presentation.common.User;

class ChatPresenter {
  private final ChatView chatView;
  private SubscribeOnChatCommand chatCommand;
  private SendMessageCommand sendMessageCommand;
  private GetLoggedUserQuery getLoggedUserQuery;

  public ChatPresenter(ChatView chatView,
                       SubscribeOnChatCommand chatCommand,
                       SendMessageCommand sendMessageCommand,
                       GetLoggedUserQuery getLoggedUserQuery) {
    this.chatView = chatView;
    this.chatCommand = chatCommand;
    this.sendMessageCommand = sendMessageCommand;
    this.getLoggedUserQuery = getLoggedUserQuery;
  }

  public void subscribeForMessageReceived() {
    chatCommand.execute(new OnMessageReceive() {
      @Override
      public void onReceived(MessageDto dto) {
        Message message = new Message(dto.sender, dto.text);
        chatView.addMessage(message);
      }
    });
  }

  public void sendMessage(Message message) {
    sendMessageCommand.execute(new MessageDto(message.getSender(), message.getText()));
  }

  public User getUser() {
    UserDto userDto = getLoggedUserQuery.execute();

    return new User(userDto.getUsername());
  }
}
