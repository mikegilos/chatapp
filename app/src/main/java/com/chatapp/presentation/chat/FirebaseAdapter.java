package com.chatapp.presentation.chat;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.chatapp.R;
import com.chatapp.presentation.common.User;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FirebaseAdapter extends FirebaseRecyclerAdapter<Message, MessageViewHolder> {

  private User user;

  private static FirebaseRecyclerOptions<Message> createOptions() {
    return new FirebaseRecyclerOptions.Builder<Message>()
        .setQuery(createQuery(), createParser())
        .build();
  }

  private static DatabaseReference createQuery() {
    return FirebaseDatabase.getInstance().getReference().child("messages");
  }

  private static SnapshotParser<Message> createParser() {
    return new SnapshotParser<Message>() {
      @Override
      public Message parseSnapshot(DataSnapshot dataSnapshot) {
        Message message = dataSnapshot.getValue(Message.class);
        if (message != null) {
          message.setId(dataSnapshot.getKey());
        }
        return message;
      }
    };
  }

  public FirebaseAdapter(final LinearLayoutManager layoutManager, User user) {
    super(createOptions());
    this.user = user;

    registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
      @Override
      public void onItemRangeInserted(int positionStart, int itemCount) {
        super.onItemRangeInserted(positionStart, itemCount);
        int friendlyMessageCount = getItemCount();
        int lastVisiblePosition = layoutManager.findLastCompletelyVisibleItemPosition();
        // If the recycler view is initially being loaded or the user is at the bottom of the list, scroll
        // to the bottom of the list to show the newly added message.
        if (lastVisiblePosition == -1 ||
            (positionStart >= (friendlyMessageCount - 1) && lastVisiblePosition == (positionStart - 1))) {
          layoutManager.scrollToPosition(positionStart);
        }
      }
    });
  }


  @Override
  protected void onBindViewHolder(@NonNull MessageViewHolder viewHolder,
                                  int position,
                                  @NonNull Message message) {
    viewHolder.update(message, isOwnUser(message.getSender()));
  }

  private boolean isOwnUser(String sender) {
    return user.isSame(sender);
  }

  @NonNull
  @Override
  public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
    return new MessageViewHolder(inflater.inflate(R.layout.item_message, viewGroup, false));
  }

  @Override
  public void onDataChanged() {
    // Called each time there is a new data snapshot. You may want to use this method
    // to hide a loading spinner or check for the "no documents" state and update your UI.
    // ...
  }

  @Override
  public void onError(DatabaseError e) {
    // Called when there is an error getting data. You may want to update
    // your UI to display an error message to the user.
    // ...
  }
}
