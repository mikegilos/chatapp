package com.chatapp.presentation.chat;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import com.chatapp.persistence.UserSharedPref;
import com.chatapp.R;
import com.chatapp.application.chat.SendMessageCommand;
import com.chatapp.application.chat.SubscribeOnChatCommand;
import com.chatapp.application.plugins.ChatService;
import com.chatapp.application.user.GetLoggedUserQuery;
import com.chatapp.infrastructure.firebase.FakeFirebase;
import com.chatapp.infrastructure.firebase.FirebaseService;
import com.chatapp.presentation.common.ToolbarView;
import com.chatapp.presentation.common.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChatFragment extends Fragment implements ChatView {

  private ChatPresenter chatPresenter;

  private MessageRecyclerAdapter messageAdapter;

  private FirebaseAdapter firebaseAdapter;

  private ChatService chatService;

  private User user;

  @BindView(R.id.recyclerView)
  RecyclerView adapterRecyclerView;

  @BindView(R.id.messageText)
  EditText messageText;

  public ChatFragment() {
    // Required empty public constructor
  }

  @Override public void onAttach(Context context) {
    super.onAttach(context);

    if (context instanceof ToolbarView) {
      ToolbarView toolbarView = (ToolbarView) context;
      toolbarView.enableLogoutButton(true);
    } else {
      throw new RuntimeException(context.toString()
                                         + " must implement ToolbarView");
    }

//    AppCompatActivity activity = (AppCompatActivity) getActivity();
//    Toolbar toolbar = (Toolbar) activity.findViewById(R.id.toolbar);
//    Button logout = toolbar.findViewById(R.id.logout_btn);
//    logout.setVisibility(View.VISIBLE);
  }

  @Override
  public View onCreateView(LayoutInflater inflater,
                           ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_chat, container, false);
    ButterKnife.bind(this, view);
    return view;
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    this.chatService = useRealFirebase(true);

    SubscribeOnChatCommand chatCommand = new SubscribeOnChatCommand.Implementation(chatService);
    SendMessageCommand sendMessageCommand = new SendMessageCommand.Implementation(chatService);
    GetLoggedUserQuery getLoggedUserQuery =
        new GetLoggedUserQuery.Implementation(new UserSharedPref((AppCompatActivity) getActivity()));

    chatPresenter = new ChatPresenter(this,
        chatCommand, sendMessageCommand, getLoggedUserQuery);

    user = chatPresenter.getUser();

    initRecyclerView();
  }

  /*
    FirebaseService uses the real implementation and syncs to the firebase db.

    FakeFirebase implementation only triggers the SubscribeOnChatCommand when SendMessageCommand is
    executed and does not sync to firebase db.
    This is for testability purposes and quick prototyping/wiring without using real implementation.
   */
  private ChatService useRealFirebase(boolean choice) {
    return choice ? new FirebaseService() : FakeFirebase.Instance;
  }

  private void initRecyclerView() {
    LinearLayoutManager layoutManager = createLayoutManager(getContext());

    initFirebaseAdapter(layoutManager);
    initMessageAdapter();

    adapterRecyclerView.setLayoutManager(layoutManager);

    /*
      You have an option to use either the
      FirebaseAdapter or MessageRecyclerAdapter

      FirebaseAdapter uses the firebaseUI implementation. It bypasses the Application Usecases
      (SubscribeOnChatCommand and SendMessageCommand) and directly syncs to the Firebase db.
      https://github.com/firebase/FirebaseUI-Android/tree/master/database

      MessageRecyclerAdapter uses the Firebase db queries and listening for the child added events.
      While adhering to the Application Usecases(SubscribeOnChatCommand and SendMessageCommand).
     */

    adapterRecyclerView.setAdapter(firebaseAdapter);
//    adapterRecyclerView.setAdapter(messageAdapter);
  }

  private void initMessageAdapter() {
    messageAdapter = new MessageRecyclerAdapter(MessageRecyclerAdapter.Empty(), user);
    chatPresenter.subscribeForMessageReceived();
  }

  private LinearLayoutManager createLayoutManager(Context context) {
    LinearLayoutManager layoutManager = new LinearLayoutManager(context);
    layoutManager.setStackFromEnd(true);
    return layoutManager;
  }

  private void initFirebaseAdapter(LinearLayoutManager layoutManager) {
    firebaseAdapter = new FirebaseAdapter(layoutManager, user);
  }

  @OnClick(R.id.send_btn)
  public void sendMessage() {
    Message message = new Message(user.getUsername(), messageText.getText().toString());
    chatPresenter.sendMessage(message);

    clearMessageInput();
    closeSoftKeyboard();
  }

  private void clearMessageInput() {
    messageText.getText().clear();
  }

  @Override
  public void onStart() {
    super.onStart();
    firebaseAdapter.startListening();
  }

  @Override
  public void onStop() {
    super.onStop();
    firebaseAdapter.stopListening();
  }

  @Override
  public void addMessage(Message message) {
    Log.d("App", "addMessage "+ message);
    messageAdapter.addMessage(message);
    scrollToLastMessage();
  }

  private void closeSoftKeyboard() {
    messageText.onEditorAction(EditorInfo.IME_ACTION_DONE);
  }

  private void scrollToLastMessage() {
    adapterRecyclerView.smoothScrollToPosition(messageAdapter.getItemCount() - 1);
  }
}
