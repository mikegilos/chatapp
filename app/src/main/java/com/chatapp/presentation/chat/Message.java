package com.chatapp.presentation.chat;

public class Message {
  private String sender;
  private String text;
  private String id;

  public Message() {}

  public Message(String sender, String text) {
    this.sender = sender;
    this.text = text;
  }

  public String getSender() {
    return sender;
  }

  public String getText() {
    return text;
  }

  public void setId(String id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return String.format("%s %s", sender, text);
  }
}
