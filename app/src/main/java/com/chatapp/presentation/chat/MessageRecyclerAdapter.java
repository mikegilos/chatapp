package com.chatapp.presentation.chat;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.chatapp.R;
import com.chatapp.presentation.common.User;

import java.util.ArrayList;
import java.util.List;

public class MessageRecyclerAdapter extends RecyclerView.Adapter<MessageViewHolder> {

  static List<Message> DummyData() {
    List<Message> messageList = new ArrayList<>();
    messageList.add(new Message("Hi!", "Cai"));
    messageList.add(new Message("Hello!", "Pof"));
    messageList.add(new Message("Sup!", "Pofay"));
    messageList.add(new Message("Nice!", "Nene"));
    messageList.add(new Message("Cool!", "Bro"));
    messageList.add(new Message("Attack!", "Sup"));
    messageList.add(new Message("Danger!", "Andre"));
    messageList.add(new Message("Warning!", "Kobe"));

    return messageList;
  }

  static List<Message> Empty() {
    return new ArrayList<>();
  }

  private List<Message> messageList;
  private User user;

  MessageRecyclerAdapter(List<Message> messageList, User user) {
    this.messageList = messageList;
    this.user = user;
    Log.d("App", "message size: " + messageList.size());
  }

  @NonNull
  @Override
  public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    return new MessageViewHolder(inflater.inflate(R.layout.item_message, parent, false));

  }

  @Override
  public void onBindViewHolder(@NonNull MessageViewHolder holder, int position) {
    Message message = messageList.get(position);
    holder.update(message, isOwnUser(message.getSender()));
  }

  private boolean isOwnUser(String sender) {
    return user.isSame(sender);
  }

  @Override
  public int getItemCount() {
    return messageList.size();
  }

  public void addMessage(Message message) {
    messageList.add(message);
    notifyDataSetChanged();
  }
}
