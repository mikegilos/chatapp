package com.chatapp.presentation.chat;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chatapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessageViewHolder extends RecyclerView.ViewHolder {

  @BindView(R.id.messageLayout)
  LinearLayout parent;

  @BindView(R.id.messageText)
  TextView messageText;

  @BindView(R.id.username)
  TextView username;

  MessageViewHolder(View itemView) {
    super(itemView);
    ButterKnife.bind(this, itemView);
  }

  public void update(Message message, boolean isOwnUser) {
    String text = message.getText();
    String sender = isOwnUser ? "You" : message.getSender();
    int alignment = isOwnUser ? Gravity.END : Gravity.START;

    Log.d("App", String.format("%s %s %s", sender, text, alignment));

    messageText.setText(text);
    username.setText(sender);
    parent.setGravity(alignment);
  }
}
