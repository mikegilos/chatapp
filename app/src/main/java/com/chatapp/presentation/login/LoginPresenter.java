package com.chatapp.presentation.login;

import android.util.Log;

import com.chatapp.application.common.UserDto;
import com.chatapp.application.login.LoginQuery;

public class LoginPresenter {
    private LoginView loginView;
    private LoginQuery loginQuery;

    LoginPresenter(LoginView loginView, LoginQuery loginQuery) {
        this.loginView = loginView;
        this.loginQuery = loginQuery;
    }

    public void login(String username, String password) {
        boolean success = loginQuery.execute(new UserDto(username, password));
        if (success)
            loginView.allowAccess();
        else
            loginView.showError();
        Log.d("App", username + " " + password);
    }
}
