package com.chatapp.presentation.login;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chatapp.persistence.UserSharedPref;
import com.chatapp.R;
import com.chatapp.application.login.LoginQuery;
import com.chatapp.application.plugins.UserStore;
import com.chatapp.presentation.chat.ChatFragment;
import com.chatapp.presentation.component.CustomInputText;
import com.chatapp.presentation.navigation.NavigationView;
import com.chatapp.presentation.signup.SignupFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginFragment extends Fragment implements LoginView {

  private NavigationView navigationView;
  private LoginPresenter loginPresenter;

  @BindView(R.id.username)
  CustomInputText usernameInputText;

  @BindView(R.id.password)
  CustomInputText passwordInputText;

  public LoginFragment() {
    // Required empty public constructor
  }


  @Override
  public View onCreateView(LayoutInflater inflater,
                           ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_login, container, false);
    ButterKnife.bind(this, view);
    return view;
  }

  @Override
  public void onViewCreated(@NonNull View view,
                            @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    UserStore userStore = new UserSharedPref((AppCompatActivity) getActivity());
    LoginQuery loginQuery = new LoginQuery.Implementation(userStore);
    loginPresenter = new LoginPresenter(this, loginQuery);
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof NavigationView) {
      navigationView = (NavigationView) context;
    } else {
      throw new RuntimeException(context.toString()
                                         + " must implement NavigationView");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    navigationView = null;
  }

  @OnClick(R.id.signup)
  public void signup() {
    Log.d("App", "signup clicked");
    navigationView.OnNavigateToScreen(new SignupFragment());
  }

  @OnClick(R.id.login)
  public void login() {
    String username = usernameInputText.getText();
    String password = passwordInputText.getText();
    loginPresenter.login(username, password);
  }

  @Override
  public void allowAccess() {
    navigationView.OnNavigateToScreen(new ChatFragment());
  }

  @Override
  public void showError() {
    usernameInputText.enableErrorPrompt(true);
    passwordInputText.enableErrorPrompt(true);
  }
}
