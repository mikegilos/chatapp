package com.chatapp.presentation.login;

public interface LoginView {
    void allowAccess();
    void showError();
}
