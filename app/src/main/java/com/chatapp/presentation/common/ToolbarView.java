package com.chatapp.presentation.common;

public interface ToolbarView {
  void enableLogoutButton(boolean enable);
}
