package com.chatapp.presentation.common;

public class User {
  private String username;

  public User(String username) {
    this.username = username;
  }

  public String getUsername() {
    return username;
  }

  public boolean isSame(String othername) {
    return username.equals(othername);
  }
}
