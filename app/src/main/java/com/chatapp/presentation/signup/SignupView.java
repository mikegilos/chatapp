package com.chatapp.presentation.signup;

public interface SignupView {
  void showError();

  void allowAccess();
}
