package com.chatapp.presentation.signup;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chatapp.persistence.UserSharedPref;
import com.chatapp.R;
import com.chatapp.application.plugins.UserStore;
import com.chatapp.application.signup.SignupQuery;
import com.chatapp.presentation.chat.ChatFragment;
import com.chatapp.presentation.component.CustomInputText;
import com.chatapp.presentation.login.LoginFragment;
import com.chatapp.presentation.navigation.NavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignupFragment extends Fragment implements SignupView {

  private NavigationView navigationView;
  private SignupPresenter signupPresenter;

  @BindView(R.id.username)
  CustomInputText usernameInputText;

  @BindView(R.id.password)
  CustomInputText passwordInputText;

  public SignupFragment() {
    // Required empty public constructor
  }


  @Override
  public View onCreateView(LayoutInflater inflater,
                           ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_signup, container, false);
    ButterKnife.bind(this, view);
    return view;
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    UserStore userStore = new UserSharedPref((AppCompatActivity) getActivity());
    SignupQuery.Implementation signupQuery = new SignupQuery.Implementation(userStore);
    signupPresenter = new SignupPresenter(this, signupQuery);
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof NavigationView) {
      navigationView = (NavigationView) context;
    } else {
      throw new RuntimeException(context.toString()
                                         + " must implement NavigationView");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    navigationView = null;
  }

  @OnClick(R.id.login)
  public void login() {
    navigationView.OnNavigateToScreen(new LoginFragment());
  }

  @OnClick(R.id.signup)
  public void signup() {
    String username = usernameInputText.getText();
    String password = passwordInputText.getText();
    signupPresenter.signup(username, password);
  }

  @Override public void showError() {
    usernameInputText.enableErrorPrompt(true);
    passwordInputText.enableErrorPrompt(true);
  }

  @Override public void allowAccess() {
    navigationView.OnNavigateToScreen(new ChatFragment());
  }
}
