package com.chatapp.presentation.signup;

import android.util.Log;

import com.chatapp.application.common.UserDto;
import com.chatapp.application.signup.SignupQuery;

public class SignupPresenter {
  private final SignupView signupView;
  private final SignupQuery signupQuery;

  public SignupPresenter(SignupView signupView, SignupQuery signupQuery) {
    this.signupView = signupView;
    this.signupQuery = signupQuery;
  }

  public void signup(String username, String password) {
    boolean success = signupQuery.execute(new UserDto(username, password));
    if (success)
      signupView.allowAccess();
    else
      signupView.showError();
    Log.d("App", username + " " + password);
  }
}
