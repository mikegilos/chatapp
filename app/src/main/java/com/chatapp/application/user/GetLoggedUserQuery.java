package com.chatapp.application.user;

import com.chatapp.application.common.UserDto;
import com.chatapp.application.plugins.UserStore;

public interface GetLoggedUserQuery {
  UserDto execute();

  class Implementation implements GetLoggedUserQuery {
    private UserStore userStore;

    public Implementation(UserStore userStore) {
      this.userStore = userStore;
    }

    @Override
    public UserDto execute() {
      return userStore.getLoggedUser();
    }
  }
}
