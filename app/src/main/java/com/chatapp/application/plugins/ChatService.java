package com.chatapp.application.plugins;

import com.chatapp.application.chat.MessageDto;
import com.chatapp.application.chat.OnMessageReceive;

public interface ChatService {
  void subscribe(OnMessageReceive callback);
  void send(MessageDto message);
}
