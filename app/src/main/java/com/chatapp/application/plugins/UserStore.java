package com.chatapp.application.plugins;

import com.chatapp.application.common.UserDto;

public interface UserStore {
  boolean contains(UserDto userDto);
  void save(UserDto dto);
  void setLoggedUser(UserDto userDto);
  UserDto getLoggedUser();
}
