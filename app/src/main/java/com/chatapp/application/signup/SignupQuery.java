package com.chatapp.application.signup;

import com.chatapp.application.common.UserDto;
import com.chatapp.application.plugins.UserStore;
import com.chatapp.domain.Authentication;

public interface SignupQuery {
  boolean execute(UserDto model);

  class Implementation implements SignupQuery {
    private final UserStore userStore;

    public Implementation(UserStore userStore) {
      this.userStore = userStore;
    }

    @Override
    public boolean execute(UserDto dto) {
      Authentication auth = new Authentication();

      boolean isValid = auth.isValid(dto.getUsername(), dto.getPassword());

      userStore.save(dto);

      return isValid;
    }
  }
}
