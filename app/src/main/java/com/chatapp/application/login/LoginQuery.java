package com.chatapp.application.login;

import com.chatapp.application.common.UserDto;
import com.chatapp.application.plugins.UserStore;
import com.chatapp.domain.Authentication;

public interface LoginQuery {
  boolean execute(UserDto model);

  class Implementation implements LoginQuery {
    private final UserStore userStore;

    public Implementation(UserStore userStore) {
      this.userStore = userStore;
    }

    @Override
    public boolean execute(UserDto dto) {
      Authentication auth = new Authentication();

      boolean isValid = auth.isValid(dto.getUsername(), dto.getPassword());
      boolean exist = userStore.contains(dto);

      userStore.setLoggedUser(dto);

      return isValid && exist;
    }
  }
}
