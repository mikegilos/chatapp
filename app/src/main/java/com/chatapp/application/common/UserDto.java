package com.chatapp.application.common;

public class UserDto {
  public static UserDto ERROR = new UserDto("", "");

  private final String username;
  private final String password;

  public UserDto(String username, String password) {
    this.username = username;
    this.password = password;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }
}
