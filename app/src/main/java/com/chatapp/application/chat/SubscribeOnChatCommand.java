package com.chatapp.application.chat;

import com.chatapp.application.plugins.ChatService;

public interface SubscribeOnChatCommand {
  void execute(OnMessageReceive callback);

  class Implementation implements SubscribeOnChatCommand {

    private ChatService chatService;
    private OnMessageReceive viewCallback;

    public Implementation(ChatService chatService) {
      this.chatService = chatService;
    }

    @Override
    public void execute(OnMessageReceive viewCallback) {
      this.viewCallback = viewCallback;
      this.chatService.subscribe(new OnMessageReceive() {
        @Override
        public void onReceived(MessageDto dto) {
          Implementation.this.viewCallback.onReceived(dto);
        }
      });
    }
  }
}
