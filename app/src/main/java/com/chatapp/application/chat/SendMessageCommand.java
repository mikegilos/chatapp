package com.chatapp.application.chat;

import com.chatapp.application.plugins.ChatService;

public interface SendMessageCommand {
  void execute(MessageDto dto);

  class Implementation implements SendMessageCommand {
    private ChatService chatService;

    public Implementation(ChatService chatService) {
      this.chatService = chatService;
    }

    @Override
    public void execute(MessageDto dto) {
      this.chatService.send(dto);
    }
  }
}
