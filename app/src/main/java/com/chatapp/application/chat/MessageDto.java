package com.chatapp.application.chat;

public class MessageDto {
  public String sender;
  public String text;
  public String id;

  public MessageDto() {}

  public MessageDto(String sender, String text) {
    this.sender = sender;
    this.text = text;
  }

  public void setId(String id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return String.format("%s %s", sender, text);
  }
}
