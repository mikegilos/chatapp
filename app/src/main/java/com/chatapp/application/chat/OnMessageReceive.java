package com.chatapp.application.chat;

public interface OnMessageReceive {
  void onReceived(MessageDto dto);
}
