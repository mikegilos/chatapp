package com.chatapp.domain;

public class Authentication {
  private static int MIN_LENGTH = 8;
  private static int MAX_LENGTH = 16;

  public Authentication() {
  }

  public boolean isValid(String username, String password) {
    return isNotEmpty(username)
            && isNotEmpty(password)
              && hasRequiredRangeOfCharLength(username)
                && hasRequiredRangeOfCharLength(password);
  }

  private boolean isNotEmpty(String str) {
    return !(str == null || str.isEmpty());
  }

  private boolean hasRequiredRangeOfCharLength(String str) {
    return str.length() >= MIN_LENGTH && str.length() <= MAX_LENGTH;
  }
}
