package com.chatapp.infrastructure.firebase;

import com.chatapp.application.chat.MessageDto;
import com.chatapp.application.chat.OnMessageReceive;
import com.chatapp.application.plugins.ChatService;

public class FakeFirebase implements ChatService {

  public static ChatService Instance = new FakeFirebase();

  private OnMessageReceive messageReceive;

  @Override
  public void subscribe(OnMessageReceive callback) {
    this.messageReceive = callback;
  }

  @Override
  public void send(MessageDto messageDto) {
    messageReceive.onReceived(messageDto);
  }
}
