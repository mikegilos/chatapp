package com.chatapp.infrastructure.firebase;

import android.util.Log;

import com.chatapp.application.chat.MessageDto;
import com.chatapp.application.chat.OnMessageReceive;
import com.chatapp.application.plugins.ChatService;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class FirebaseService implements ChatService {

  private static final String MESSAGES_CHILD = "messages";

  private DatabaseReference databaseReference;

  public FirebaseService() {
    configureFirebaseDb();
  }

  private void configureFirebaseDb() {
    databaseReference = FirebaseDatabase.getInstance().getReference();
  }

  public DatabaseReference createQuery() {
    return databaseReference.child(MESSAGES_CHILD);
  }

  @Override
  public void subscribe(final OnMessageReceive callback) {
    Query query = FirebaseDatabase.getInstance()
        .getReference()
        .child("messages")
        .limitToLast(50);

    ChildEventListener childEventListener = new ChildEventListener() {
      @Override
      public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
        MessageDto messageDto = dataSnapshot.getValue(MessageDto.class);
        Log.d("App", "onChildAdded " + messageDto.toString());
        callback.onReceived(messageDto);
      }

      @Override
      public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {}

      @Override
      public void onChildRemoved(DataSnapshot dataSnapshot) {}

      @Override
      public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {}

      @Override
      public void onCancelled(DatabaseError databaseError) {}
    };
    query.addChildEventListener(childEventListener);
  }

  @Override
  public void send(MessageDto message) {
    databaseReference.child(MESSAGES_CHILD)
        .push().setValue(message);
  }
}
